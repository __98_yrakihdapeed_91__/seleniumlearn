package codes;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class HelloSelenium {

	public static void main(String[] args) {
		Path p=Paths.get("");
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", p.toAbsolutePath().toString()+"/src/Resources/geckodriver.exe");
		WebDriver driver=new FirefoxDriver();
		try {
			
			driver.get("https://www.seleniumhq.org/");
			WebElement downloadbox=driver.findElement(By.className("downloadBox"));
			if(downloadbox.isDisplayed()) {
				System.out.println("Navigation Successful");
				System.out.println(driver.getCurrentUrl());
				//By.xpath("//UL[@id='sitemap']//a")
				//By.cssSelector("ul#sitemap a")
				List<WebElement> allSitemapList=driver
						.findElement(By.id("sitemap"))
						.findElements(By.tagName("a"));
				
				System.out.println(allSitemapList.size());
				for(WebElement link:allSitemapList) {
					System.out.println(link.getText());
				}
				downloadbox.click();
				WebElement stanDaloneServer=driver.findElement(By.xpath("//div[@id='mainContent']//h3[1]"));
				
				if(stanDaloneServer
						.getText()
						.toLowerCase()
						.equals("selenium standalone server")
						) {
					System.out.println("Navigation to download page is successful");
				}else {
					System.out.println("Navigation to download page is fail");
				}
				Thread.sleep(5000);
			}else {
				System.out.println("Navigation Fail");
			}
			//driver.close();
			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//System.out.println(e.getMessage());
		}

	}

}

