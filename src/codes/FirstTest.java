package codes;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FirstTest {
	private Path p = Paths.get("");
	private WebDriver driver;
	private WebElement downloadbox;
	private String url="https://www.seleniumhq.org/";
	
	@BeforeClass
	public void beforeClass() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", p.toAbsolutePath().toString() + "/src/Resources/geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
		driver = new FirefoxDriver();
		driver.get(url);
		driver.manage().window().maximize();
		Thread.sleep(5000);
	}
	
	@Test
	public void test() {
		Assert.assertEquals(driver.getCurrentUrl(), url);
		downloadbox = driver.findElement(By.className("downloadBox"));
		Assert.assertEquals(downloadbox.isDisplayed(), true);
		System.out.println("Navigation Successful");
		System.out.println();
		List<WebElement> allSitemapList = driver.findElement(By.id("sitemap")).findElements(By.tagName("a"));

		System.out.println(allSitemapList.size());
		for (WebElement link : allSitemapList) {
			Assert.assertNotEquals(link.getText(), "");
		}
		
	}
	
	@Test
	public void test2() throws InterruptedException {
		downloadbox.click();
		WebElement stanDaloneServer = driver.findElement(By.xpath("//div[@id='mainContent']//h3[1]"));
		Assert.assertEquals(stanDaloneServer.getText().toLowerCase(), "selenium standalone server");
		Thread.sleep(5000);
	}
	
	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
