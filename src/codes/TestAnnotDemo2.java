package codes;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestAnnotDemo2 {
	
	@BeforeMethod
	public void before() {
		System.out.println("Test02:I Run Before Each Test");
	}
	@AfterMethod
	public void after() {
		System.out.println("Test02:I Run After Each Test");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("Test02:I Run Before A Test Class I mean before all the test executed of a Class");
	}
	@AfterClass
	public void afterClass() {
		System.out.println("Test02:I Run After A Test Class I mean after all the test executed of a Class");
	}
	
	
	@Test
	public void test01() {
		System.out.println("Test 01");
	}

	@Test
	public void test02() {
		System.out.println("Test 02");
	}

	@Test
	public void test03() {
		System.out.println("Test 03");
	}

	@Test
	public void test04() {
		System.out.println("Test 04");
	}
}
