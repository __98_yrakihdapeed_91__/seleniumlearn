package codes;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class SuiteInit {
  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("Run Before Suite");
  }
  @AfterSuite
  public void afterSuite() {
	  System.out.println("Run Before Suite");
  }
  @BeforeTest
  public void beforeTest() {
	  System.out.println("Hi I am before Test");
  }
  @AfterTest
  public void afterTest() {
	  System.out.println("Hi I am before Test");
  }
}
